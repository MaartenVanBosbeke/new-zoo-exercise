package com.vdab.services;

import com.vdab.domain.Zebra;
import com.vdab.repositories.ZebraRepository;

import java.util.List;

public class ZebraService {

    ZebraRepository zebraRepository = new ZebraRepository();

    public void addZebra(Zebra zebra){
        zebraRepository.addZebra(zebra);

    }

    public List<Zebra> getZebraList(){
        return zebraRepository.getZebra();
    }

}
