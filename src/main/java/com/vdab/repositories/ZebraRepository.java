package com.vdab.repositories;

import com.vdab.domain.Zebra;
import java.util.ArrayList;
import java.util.List;

public class ZebraRepository {

    private static ArrayList<Zebra> zebraList = new ArrayList<>();

    public void addZebra(Zebra zebra) {
        zebraList.add(zebra);
    }

    public List<Zebra> getZebra(){
        return zebraList;
    }

}
