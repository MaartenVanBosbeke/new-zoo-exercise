package com.vdab.domain;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.Scanner;

@ToString(callSuper = true) //use ctrl+space between ()
@Data
@SuperBuilder

//SubClass
public class Zebra extends Animal {
    @ToString.Include //needed to include static final fields, only works on line directly below
    static final int stripes = 3;
    @ToString.Exclude //only works on line directly below
    private String tattoo;
    private String danceMoves;
    private static Scanner scan = new Scanner(System.in);

    public String getDanceMoves() {
        return "BoogieDance";
    }


    public static Zebra createZebra(){ //FACTORY PATTERN (Creational category), method that creates object
        Zebra.ZebraBuilder zebra = Zebra.builder(); //start the builder
        System.out.println("Give the name");
        String name = scan.next();
        zebra.name(name); //add the scanned name to the builder
        System.out.println("give the age");
        int age = scan.nextInt();
        zebra.age(age); //add the scanned age to the builder
        Zebra zebra1 = zebra.build(); //create a zebra, build it
        return zebra1;
    }




}

