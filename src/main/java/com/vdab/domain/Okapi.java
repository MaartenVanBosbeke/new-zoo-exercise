package com.vdab.domain;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
//SubClass
public class Okapi extends Animal {

    private int stripes;
    private boolean talks;
    private String language;


}
