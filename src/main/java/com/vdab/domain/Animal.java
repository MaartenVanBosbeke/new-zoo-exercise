package com.vdab.domain;

//Super class

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public abstract class Animal {

    private String name;
    private int eyes;
    private int legs;
    private boolean tail;
    private String sound;
    private int age;




}
