package com.vdab;

import com.vdab.domain.Zebra;
import com.vdab.services.ZebraService;

import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;


public class Main {

    private static Scanner scan = new Scanner(System.in);
    private static ZebraService zebraService = new ZebraService();

    public static void main(String[] args) {
        //zoo project
        System.out.println("Welcome to our zoo");

        while(true) {
            showInitialOptions();
            chooseOption();

            System.out.println("Do you want to continue? (y/n)"); //clear console
            if(scan.next().equals("y")) {
                System.out.println("Clear"); //if y, clear console
            }else{
                break; //if not y, quit program
            }
        }

    }

    private static void showInitialOptions() {
        System.out.println("What do you want to do?" +
                "\n\t1. Print zebra list" +
                "\n\t2. Add a zebra" +
                "\n\t3. Remove a zebra");
    }

    private static void chooseOption(){
        int choice = scan.nextInt();
        switch(choice){
            case 1:
                printZebraList();
                break;
            case 2:
                addAZebra();
                break;
            case 3:
                removeZebra();
                break;
            default:
                System.out.println("Wrong input, returning to main menu");
                showInitialOptions();
                chooseOption();
                break;
        }
    }

    private static void addAZebra() {
        Zebra zebra1 = Zebra.createZebra(); //create a new zebra called zebra1, using the createZebra method
        zebraService.addZebra(zebra1); //add the created zebra to the arraylist via service
    }

    private static void removeZebra() {
        printZebraList();
        System.out.println("which zebra do you want to remove?");
        int i = scan.nextInt();
        zebraService.getZebraList().remove(i - 1);
        System.out.println("zebra " + i + " has been removed");
    }

    private static void printZebraList() {
        System.out.println("List of all zebras");
        int i = 1;
        for (Zebra z : zebraService.getZebraList()) {
            System.out.println(i + " " + z + "\n-----");
            i++;
        }
    }

}

//        zebraService.getZebraList().stream().forEach( zebraItem -> {System.out.println("index: " + i + " " + zebraItem + "\n-------");
//            i++;});
//    }


    //IntStream stream = new Random().ints(1, 66); //never ending stream of random ints between 1 up to 66 (excl)
//stream.forEach(value -> System.out.print(value + " "));


//    public static Zebra createZebra(){ //FACTORY PATTERN, method that creates object
//        Zebra.ZebraBuilder zebra = Zebra.builder(); //start the builder
//        System.out.println("Give the name");
//        String name = scan.next();
//        zebra.name(name); //add the scanned name to the builder
//        System.out.println("give the age");
//        int age = scan.nextInt();
//        zebra.age(age); //add the scanned age to the builder
//        Zebra zebra1 = zebra.build(); //create a zebra, build it
//        return zebra1;
//    }
    // => can also be put in the zebra class



//https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.javaguides.net%2F2020%2F07%2Fthree-tier-three-layer-architecture-in-spring-mvc-web-application.html&psig=AOvVaw3OSR7IoxmNimXhFvvbAKtf&ust=1619690333319000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPib-fvWoPACFQAAAAAdAAAAABA8
// creational design patterns: https://www.javatpoint.com/creational-design-patterns
    //Factory Method Pattern
    //Abstract Factory Pattern
    //Singleton Pattern
    //Prototype Pattern
    //Builder Pattern
    //Object Pool Pattern














//TODO: write what the code is supposed to do
//TODO: methods in subclasses to call in main via services
//TODO: arraylists / db
//TODO: sql
//TODO: jdbc: add, update, search, remove
//TODO: @Cleanup testen
//TODO:
//TODO:
//TODO: GIT push commit, branching and merging
//TODO:
//TODO:
//TODO: lambda's
//TODO: tests
//TODO: design patterns bekijken
//TODO: factory and singleton patterns bekijken